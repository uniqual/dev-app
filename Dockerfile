# defines the base layer for the container, in this case, a version of OpenJDK 11
FROM adoptopenjdk/openjdk11:jdk-11.0.2.9-slim
# sets the working directory to /opt/. Every subsequent instruction runs from within that folder
WORKDIR /opt

# is used to set an environment variable
ENV PORT 8080

EXPOSE 8080

# COPY copies the jar files from the /target/ into the /opt/ directory inside the container
COPY target/*.jar /opt/app.jar

# executes java $JAVA_OPTS -jar app.jar inside the container
ENTRYPOINT exec java $JAVA_OPTS -jar app.jar
