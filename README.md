# Run Application

* JVM Options 

`-Dspring.profiles.active=dev`

# Containerising the application

1.1 Create Dockerfile

``` 
 FROM adoptopenjdk/openjdk11:jdk-11.0.2.9-slim
 WORKDIR /opt
 ENV PORT 8080
 EXPOSE 8080
 COPY target/*.jar /opt/app.jar
 ENTRYPOINT exec java $JAVA_OPTS -jar app.jar
```

1.2. Build application docker image 

```
  docker build -t spring-app -f Dockerfie .
```

1.3. Build mysql docker image

```
docker pull mysql:5.7
docker run --name mysql-standalone -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=dev.app -e MYSQL_USER=user -e MYSQL_PASSWORD=password -p 2012:3306 -d mysql:5.7
```

1.4. Run application image and link with mysql container 

```
docker run -d -p 8080:8080 --name spring-app --link mysql-standalone:mysql spring-app
```