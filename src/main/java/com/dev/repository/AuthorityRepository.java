package com.dev.repository;

import com.dev.entity.Authority;
import com.dev.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Integer> {

   Optional<Authority> findByRole(Role role);
}
