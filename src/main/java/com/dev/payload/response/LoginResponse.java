package com.dev.payload.response;

public class LoginResponse {

    private String authenticationToken;

    public LoginResponse() {
    }

    public LoginResponse(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }
}
