package com.dev.service;

import com.dev.entity.Authority;
import com.dev.exception.ResourceNotFoundException;
import com.dev.repository.AuthorityRepository;
import com.dev.security.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorityServiceImpl implements AuthorityService {

    private final AuthorityRepository authorityRepository;

    @Autowired
    public AuthorityServiceImpl(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    @Override
    public Authority getAuthority(Role role) {
        return authorityRepository.findByRole(role)
                .orElseThrow(() -> new ResourceNotFoundException("No such role " + role.name()));
    }
}
