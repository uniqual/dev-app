package com.dev.service;

import com.dev.entity.User;
import com.dev.payload.request.SignUpRequest;

import java.util.List;

public interface UserService {

    User save(User user);

    void register(SignUpRequest signUpRequest);

    List<User> getAllUsers();

    User getUserById(Integer id);

    User getUserByEmail(String email);

    User getUserByUsername(String username);

    User getUserByEmailOrUsername(String email, String username);

    Boolean existsByEmail(String email);

    Boolean existsByUsername(String username);
}
