package com.dev.service;

import com.dev.entity.Authority;
import com.dev.entity.User;
import com.dev.exception.ResourceNotFoundException;
import com.dev.payload.request.SignUpRequest;
import com.dev.repository.UserRepository;
import com.dev.security.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityService authorityService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityService authorityService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityService = authorityService;
    }

    @Override
    public User save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Authority authorityUser = authorityService.getAuthority(Role.ROLE_USER);
        user.setAuthorities(Collections.singleton(authorityUser));
        return userRepository.save(user);
    }

    @Override
    public void register(SignUpRequest signUpRequest) {
        User user = new User();
        user.setEmail(signUpRequest.getEmail());
        user.setUsername(signUpRequest.getUsername());
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));

        Authority authorityUser = authorityService.getAuthority(Role.ROLE_USER);
        user.setAuthorities(Set.of(authorityUser));
        userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found with id : " + id));
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findUserByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("User not found by email : " + email));
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findUserByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User not found by username : " + username));
    }

    @Override
    public User getUserByEmailOrUsername(String email, String username) {
        return userRepository.findUserByEmailOrUsername(email, username)
                .orElseThrow(() -> new ResourceNotFoundException("User not found by email " + email + " or username " + username));
    }

    @Override
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public Boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }
}
