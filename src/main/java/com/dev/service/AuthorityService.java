package com.dev.service;

import com.dev.entity.Authority;
import com.dev.security.Role;

public interface AuthorityService {

    Authority getAuthority(Role role);
}
