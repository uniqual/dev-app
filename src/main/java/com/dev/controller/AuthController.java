package com.dev.controller;

import com.dev.exception.ConflictException;
import com.dev.payload.request.LoginRequest;
import com.dev.payload.request.SignUpRequest;
import com.dev.payload.response.ApiResponse;
import com.dev.payload.response.LoginResponse;
import com.dev.security.jwt.JwtTokenProvider;
import com.dev.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.dev.utility.AppConstants.HEADER_AUTHORIZATION;
import static com.dev.utility.AppConstants.JWT_PREFIX;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager, UserService userService, JwtTokenProvider jwtTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<LoginResponse> authenticate(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String authToken = jwtTokenProvider.generate(authentication);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HEADER_AUTHORIZATION,  JWT_PREFIX + authToken);

        return ResponseEntity.status(HttpStatus.OK)
                .headers(httpHeaders)
                .body(new LoginResponse(authToken));
    }

    @PostMapping(value = "/signup")
    public ResponseEntity<ApiResponse> signUp(@Valid @RequestBody SignUpRequest signUpRequest) {

        if (userService.existsByEmail(signUpRequest.getEmail()).equals(true)) {
            throw new ConflictException("This email is already used");
        }

        if (userService.existsByUsername(signUpRequest.getUsername()).equals(true)) {
            throw new ConflictException("This username is already used");
        }

        userService.register(signUpRequest);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(new ApiResponse(true, "User successfully registered"));
    }
}
