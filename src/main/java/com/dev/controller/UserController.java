package com.dev.controller;

import com.dev.entity.User;
import com.dev.payload.response.UserProfileResponse;
import com.dev.security.CurrentUser;
import com.dev.security.UserPrincipal;
import com.dev.service.UserService;
import com.dev.utility.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/profile")
    public ResponseEntity<UserProfileResponse> userProfile(@CurrentUser UserPrincipal userPrincipal) {
        User user = userService.getUserById(userPrincipal.getId());
        UserProfileResponse userProfile = UserMapper.userProfileResponseOf(user);
        return ResponseEntity.status(HttpStatus.OK)
                .body(userProfile);
    }
}
