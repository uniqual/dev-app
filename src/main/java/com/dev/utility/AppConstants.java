package com.dev.utility;

public final class AppConstants {

    /**
     * Headers
     */
    public static final String HEADER_AUTHORIZATION = "Authorization";

    /**
     * Json Web Token
     */
    public static final String JWT_PREFIX = "Bearer ";

    /**
     * Pagination
     */
    public static final String DEFAULT_PAGE_NUMBER = "0";
    public static final String DEFAULT_PAGE_SIZE = "30";
    public static final int MAX_PAGE_SIZE = 50;

    private AppConstants() {

    }
}
