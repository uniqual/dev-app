package com.dev.utility;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.TimeZone;

@Component
public class AppSettings implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        setTimeZone();
    }

    private void setTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}
