package com.dev.utility.mapper;

import com.dev.entity.User;
import com.dev.payload.response.UserProfileResponse;

import java.util.List;
import java.util.stream.Collectors;

public final class UserMapper {

    private UserMapper() { }

    public static UserProfileResponse userProfileResponseOf(User user) {
        UserProfileResponse userProfileResponse = new UserProfileResponse();
        userProfileResponse.setId(user.getId());
        userProfileResponse.setEmail(user.getEmail());
        userProfileResponse.setUsername(user.getUsername());
        userProfileResponse.setJoinedAt(user.getCreatedAt());

        List<String> roles = user.getAuthorities().stream()
                .map(authority -> authority.getRole().name())
                .collect(Collectors.toList());

        userProfileResponse.setRole(roles);
        return userProfileResponse;
    }
}
