package com.dev.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler({ConflictException.class})
    public ResponseEntity<ApiError> handleConflictException(RuntimeException exception, WebRequest webRequest, HttpServletRequest request) {
        ApiError apiError = new ApiError();


        apiError.setMessage(exception.getMessage());
        apiError.setTimestamp(Instant.now());
        apiError.setPath(request.getRequestURI());

        LOGGER.error("Conflict exception was invoked on URI : {}", request.getRequestURI(), exception);

        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(apiError);
    }

    @ExceptionHandler({BadRequestException.class})
    public ResponseEntity<ApiError> handleBadRequestException(RuntimeException exception, WebRequest webRequest, HttpServletRequest request) {
        ApiError apiError = new ApiError();

        apiError.setMessage(exception.getMessage());
        apiError.setTimestamp(Instant.now());
        apiError.setPath(request.getRequestURI());

        LOGGER.error("BadRequest exception was invoked on URI : {}", request.getRequestURI(), exception);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(apiError);
    }
}
