package com.dev.security;

import com.dev.entity.User;
import com.dev.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {

        User user = userRepository.findUserByEmailOrUsername(username, username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User not found %s", username)));
        return UserPrincipal.build(user);
    }

    @Transactional
    public UserDetails loadUserById(Integer id) {

        User user = userRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User with id %s not found", id)));
        return UserPrincipal.build(user);
    }
}
