package com.dev.controller;

import com.dev.exception.GlobalExceptionHandler;
import com.dev.payload.request.LoginRequest;
import com.dev.payload.request.SignUpRequest;
import com.dev.payload.response.ApiResponse;
import com.dev.security.jwt.JwtTokenProvider;
import com.dev.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.dev.utility.AppConstants.HEADER_AUTHORIZATION;
import static com.dev.utility.AppConstants.JWT_PREFIX;
import static org.hamcrest.Matchers.startsWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class AuthControllerTest {

    private AuthController authController;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private UserService userService;

    @Mock
    private JwtTokenProvider jwtTokenProvider;


    private static String convertObjectToJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private SignUpRequest signUpRequest() {
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setEmail("user@mail.com");
        signUpRequest.setUsername("username");
        signUpRequest.setPassword("password");
        return signUpRequest;
    }

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        authController = new AuthController(authenticationManager, userService, jwtTokenProvider);
    }

    @Test
    void testAuthentication() throws Exception {

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail("username");
        loginRequest.setPassword("password");

        String jsonRequest = convertObjectToJsonString(loginRequest);

        MockMvc mockMvc = MockMvcBuilders
                .standaloneSetup(authController)
                .build();

        mockMvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest))
                .andExpect(status().isOk())
                .andExpect(header().string(HEADER_AUTHORIZATION, startsWith(JWT_PREFIX)));

        verify(authenticationManager, times(1)).authenticate(any());
        verify(jwtTokenProvider, times(1)).generate(any());
    }

    @Test
    void testSignUp() throws Exception {

        SignUpRequest signUpRequest = signUpRequest();
        ApiResponse apiResponse = new ApiResponse(true, "User successfully registered");

        String jsonRequest = convertObjectToJsonString(signUpRequest);
        String jsonResponse = convertObjectToJsonString(apiResponse);

        MockMvc mockMvc = MockMvcBuilders
                .standaloneSetup(authController)
                .build();

        mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest))
                .andExpect(status().isCreated())
                .andExpect(content().json(jsonResponse));
    }

    @Test
    public void testSignUpUsernameAlreadyUsed() throws Exception {
        SignUpRequest signUpRequest = signUpRequest();

        String jsonRequest = convertObjectToJsonString(signUpRequest);

        when(userService.existsByUsername(signUpRequest.getUsername())).thenReturn(true);

        MockMvc mockMvc = MockMvcBuilders
                .standaloneSetup(authController)
                .setControllerAdvice(new GlobalExceptionHandler())
                .build();

        mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest))
                .andExpect(status().isConflict());
    }

    @Test
    public void testSignUpEmailAlreadyUsed() throws Exception {
        SignUpRequest signUpRequest = signUpRequest();

        String jsonRequest = convertObjectToJsonString(signUpRequest);

        when(userService.existsByEmail(signUpRequest.getEmail())).thenReturn(true);

        MockMvc mockMvc = MockMvcBuilders
                .standaloneSetup(authController)
                .setControllerAdvice(new GlobalExceptionHandler())
                .build();

        mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest))
                .andExpect(status().isConflict());
    }
}