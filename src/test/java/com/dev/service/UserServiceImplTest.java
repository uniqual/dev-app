package com.dev.service;

import com.dev.entity.User;
import com.dev.payload.request.SignUpRequest;
import com.dev.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserServiceImplTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private AuthorityService authorityService;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        userService = new UserServiceImpl(userRepository, passwordEncoder, authorityService);
    }

    @Test
    void getAllUsers() {
        when(userService.getAllUsers()).thenReturn(initUsersList());

        List<User> users = userService.getAllUsers();
        assertEquals(initUsersList().size(), users.size());

        verify(userRepository, times(1)).findAll();
    }

    @Test
    void registerUser() {
        userService.register(signUpRequest());

        verify(authorityService, times(1)).getAuthority(any());
        verify(userRepository, times(1)).save(any());

    }

    private List<User> initUsersList() {
        List<User> users = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            users.add(new User());
        }
        return users;
    }

    private SignUpRequest signUpRequest() {
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setUsername("username");
        signUpRequest.setEmail("user@mail");
        signUpRequest.setPassword("Password1");
        return signUpRequest;
    }
}